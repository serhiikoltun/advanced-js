


// у кожного обьекта є Prototypeвоно дорівню або null або іншому обьекту(прототипу),
//     коли ми хочемо прочитати властивість, а її в обьекті немає то JS автоматично шукає цю властивість у прототипі.
//     Тобто ми використовуємо властивости прототипу для нашого нового обьекту, таким чином ми їх не копіюємо у новий обьект.


// super() потрібен для виклику батьківського конструктору





class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name (value) {
        this._name = value;
    }
    get name () {
        return this._name;
    }
    set age (value) {
        this._age = value;
    }
    get age () {
        return this._age;
    }
    set salary (value) {
        this._salary = value;
    }
    get salary () {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary () {
        return this._salary*3;
    }
}


const programmer1 = new Programmer("John", 23, 7000, ["Python","CSS"]);
const programmer2 = new Programmer("Mike", 27, 9000, ["Java", "JavaScript"]);
const programmer3 = new Programmer("Alise", 31, 5500, ["HTML", "C++"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);


