
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];



const block = document.createElement('div');

document.body.prepend(block);
block.id = 'root';


function renderBooks(array) {
    const listElement = document.createElement('ul');

    array.forEach((obj, indexObject) => {
        try {
            const requiredProperties = ['author', 'name', 'price'];
            for (let i = 0; i < requiredProperties.length; i++) {
                const property = requiredProperties[i];
                if (!obj.hasOwnProperty(property)) {
                    throw new SyntaxError(`Властивість ${property} у елементі масиву з індексом ${indexObject} відсутня`);
                }
            }
            const content = `<li>Author: ${obj.author}<br> Name: ${obj.name}<br>Price: ${obj.price}</li>`
            listElement.insertAdjacentHTML('beforeend', content);
            block.append(listElement);
        }
        catch (e) {
            console.log('Помилка' + ": " + e.message)
        }
    })
}


renderBooks(books)

